if (Drupal.jsEnabled) {
  $(document).ready(function() {
    $.slideshow(
      {
        container : 'slideShow1',
        loader: 'images/slideshow_loader.gif',
        linksPosition: 'top',
        linksClass: 'pagelinks',
        linksSeparator : ' | ',
        fadeDuration : 400,
        activeLinkClass: 'activeSlide',
        nextslideClass: 'nextSlide',
        prevslideClass: 'prevSlide',
        captionPosition: 'bottom',
        captionClass: 'slideCaption',
        autoplay: 5,
        images : [
          {
            src: 'images/panda/Panda.jpg',
            caption: 'Fiat Panda'
          },
          {
            src: 'images/panda/Active.jpg',
            caption: 'Panda Active'
          },
          {
            src: 'images/panda/Actual.jpg',
            caption: 'Panda Actual'
          },
          {
            src: 'images/panda/Dynamic.jpg',
            caption: 'Panda Dynamic'
          }
        ]
      }
    );
  }
}