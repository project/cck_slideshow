CCK Slideshow is a simple module which takes several existing pieces of Drupal modules and makes a slideshow from any existing imagefield defined in CCK.

Dependencies:
- CCK
- Imagefield
- Imagecache

Written by Nathan Haug: http://quicksketch.org

This Module Made by Robots: http://www.lullabot.com

Install
-------
Install all the dependecies and cck_slideshow by downloading the modules from Drupal.org. Decompress the .gz files and drop the entire uncompressed directories into 'sites/all/modules'. If this directory does not exist you may need to create it.

Enable the module and its dependencies at http://my-drupal-site.com/admin/build/modules

Configuration
-------------
The heart of CCK Slideshow is a content formatter. Formatters provide various ways of displaying field information in CCK. These formatters are used in two places in particular: the CCK display settings and in a view (provided by the views.module).

1. Create an imagefield:

- Create a new content type or edit an existing type (admin/content/types).
- Add a new field to this content type of type 'image'.
- This new imagefield MUST BE MULTIPLE VALUE to work with slideshow.
- Save your new imagefield.

2 (option a). Enable slideshow as CCK display (most common):

- Go to the 'Display fields' tab of your node type which contains the multiple value imagefield.
- Under the 'Teaser' or 'Full' drop down select for your imagefield, choose the formatter 'Slideshow'.

2 (option b). Enable slideshow as part of a View:

- Create or edit a view.
- Add your imagefield field to your view.
- Select the formatter 'Slideshow' from the select list of formatters.

3. Configure your slideshow display options.

- Go to admin/settings/cck_slideshow
- A list of all imagefields is displayed there, click 'edit slideshow formatter' on the imagefield you are using in your node type.
- Choose the imagecache preset you wish to use for the display of your slideshow.
- Configure any other settings for your slideshow.

Support
-------
If you experience a problem with cck_imagefield or have a problem, file a request or issue on the cck_imagefield queue at http://drupal.org/project/issues/cck_slideshow. DO NOT POST IN THE FORUMS. Posting in the issue queues is a direct line of communication with the module authors.